package jogo21;
import java.util.Random;

public class Embaralhar {
	private Random random = new Random();
	private Cartas cartas;

	public Embaralhar(Cartas cartas){
		this.cartas = cartas;
	}

	public int sortear() {
		return random.nextInt(cartas.valores.length);
	}
}


